import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import cookieParser from "cookie-parser";
import morgan from "morgan";

import { config, corsOptions } from "@config";
import { errorHandler, logRequest, responseHandler } from "@middleware/index";
import indexRouter from "@routes";
import loggerLoader from "@loaders/logger.loader";

export default ({ app }: { app: express.Application }) => {
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));
  app.use(cors(corsOptions));
  app.use(cookieParser());
  app.use(bodyParser.json());

  // if (config.nodeEnv === "development") {
  //   app.use(
  //     logger(
  //       ':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" - :response-time ms - :total-time[digits]',
  //     ),
  //   );
  // }

  morgan.token(
    "request",
    ({ body, params, headers, query }: express.Request, _res) =>
      JSON.stringify({ body, params, headers, query }),
  );

  morgan.token("response", (_req, { locals }: express.Response) =>
    JSON.stringify(locals),
  );

  app.use(morgan(":response", { stream: loggerLoader.stream }));
  // app.use(morgan("combined"));

  // app.use(logRequest);

  app.use(config.api.prefix, indexRouter);

  app.use(responseHandler);
  app.use(errorHandler);
};
