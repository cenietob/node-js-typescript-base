var split = require("split");
var winston = require("winston");

var logger = winston.createLogger({
  transports: [
    new winston.transports.File({
      level: "info",
      filename: "./logs/logs.log",
      json: true,
      maxsize: 5242880, //5MB
      maxFiles: 5,
      colorize: false,
    }),
    new winston.transports.File({
      level: "error",
      filename: "./logs/error.log",
      handleExceptions: true,
      json: true,
      maxsize: 5242880, //5MB
      maxFiles: 5,
      colorize: false,
    }),
  ],
  exitOnError: false,
});


logger.stream = split().on("data", function (message: string) {
  logger.info(message);
});

export default logger;
