import express from "express";

import "@loaders/process.loader";
import expressLoader from "@loaders/express.loader";

export default ({ expressApp }: { expressApp: express.Application }) => {
  expressLoader({ app: expressApp });
};
