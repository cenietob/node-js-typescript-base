process.on("unhandledRejection", (reason, p) => {
  console.log(`unhandledRejection: ${reason}`);
});

process.on("uncaughtException", (reason: any, p: any) => {
  console.log(`uncaughtException: ${reason}`);
});
