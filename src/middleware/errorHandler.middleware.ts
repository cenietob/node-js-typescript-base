import express = require("express");

import { config } from "@config/index";
import { ErrorResponse } from "@utils/utilities";
import loggerLoader from "@loaders/logger.loader";

/**
 * Error handler
 */
export const errorHandler = (
  err: ErrorResponse,
  req: any,
  res: express.Response,
  next: any,
) => {
  if (config.nodeEnv === "development") console.error(err);
  loggerLoader.error(err);

  res.status(err.statusCode || 500).json({
    success: false,
    error: err.message || err.name || "Server Error",
  });
};
