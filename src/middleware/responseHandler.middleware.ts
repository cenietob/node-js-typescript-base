import express = require("express");

import { commonErrors, serverErrors } from "@utils/constants";
import { ErrorResponse } from "@utils/utilities/index";

/**
 * Response handler for any request
 */
export const responseHandler = (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
) => {
  const { serverError, notFound } = serverErrors;
  const { responseBadFormatted, notFound: _notFound } = commonErrors;

  const {
    locals: { response },
  } = res;
  if (response) {
    if (response.headers) {
      res.set(response.headers);
    }
    if (!response.statusCode || !response.body) {
      return next(
        new ErrorResponse(serverError, responseBadFormatted, undefined, true),
      );
    }
    res.status(response.statusCode).json(response.body);
  } else {
    return next(new ErrorResponse(notFound, _notFound, undefined, true));
  }
};
