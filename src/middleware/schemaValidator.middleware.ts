import { Response, Request, NextFunction } from "express";
import { Schema } from "joi";

import { serverErrors } from "@utils/constants";
import { ErrorResponse } from "@utils/utilities/index";
import { Mutable } from "@interfaces/index";
import { getProperty, setProperty } from "@utils/utilities";

/**
 * Schema validator for incoming data (on POST/PUT/PATCH)
 * ?more info https://www.digitalocean.com/community/tutorials/node-api-schema-validation-with-joi
 *
 * @param {Schema} schema schema to be used
 * @param {keyof Request} source data source to validate
 * @returns {Function}} Returns the middleware validator
 */
export const schemaValidator = (
  schema: Schema,
  source: keyof Request = "body",
): Function => {
  const _supportedMethods = ["get", "post", "put", "patch", "delete"];

  const _validationOptions = {
    abortEarly: false, // abort after the last validation error
    allowUnknown: true, // allow unknown keys that will be ignored
    stripUnknown: true, // remove unknown keys from the validated data
  };

  return (req: Mutable<Request>, res: Response, next: NextFunction) => {
    const method = req.method.toLowerCase();

    if (_supportedMethods.includes(method) && schema) {
      const { error, value } = schema.validate(
        getProperty(req, source),
        _validationOptions,
      );
      if (error) {
        return next(
          new ErrorResponse(
            serverErrors.badRequest,
            error.details
              .map(({ message }) => message.replace(/['"]/g, "").trim())
              .join("; "),
          ),
        );
      }
      setProperty(req, source, value);
      return next();
    }
    return next();
  };
};
