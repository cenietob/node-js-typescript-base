import { Request, Response, NextFunction } from "express";
import requestIp from "request-ip";
import fs, { writeFile } from "fs";
import winston from "winston";
import { start } from "repl";

export const logRequest = (req: Request, res: Response, next: NextFunction) => {
  const myLogger = winston.createLogger({
    format: winston.format.json(),
    transports: [
      new winston.transports.File({
        filename: process.cwd() + "/project.logs",
      }),
    ],
  });
  const startTime = process.hrtime();
  const clientIp = requestIp.getClientIp(req) || "No ip";
  console.log(clientIp);
  next();
  const endTime = process.hrtime(startTime);
  myLogger.info({
    timeStamp: new Date().toLocaleString(),
    message: clientIp,
    time: endTime[0],
    time2: endTime[1] / 1000000,
  });
  // writeFile("test.json", clientIp, () => {});
};
