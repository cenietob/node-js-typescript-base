import { ServerError } from "@interfaces/index";
import { commonErrors } from "@utils/constants/messages/error.constants";

export const serverErrors: Record<string, ServerError> = Object.freeze({
  badRequest: { name: commonErrors.badRequest, code: 400 },
  unauthorized: { name: commonErrors.unauthorized, code: 401 },
  notFound: { name: commonErrors.notFound, code: 404 },
  serverError: { name: commonErrors.serverError, code: 500 },
});
