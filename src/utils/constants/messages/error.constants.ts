export const commonErrors = Object.freeze({
  badRequest: "Bad Request",
  unauthorized: "Unauthorized",
  notFound: "Not Found",
  serverError: "Internal Server Error",
  responseBadFormatted: "Backend Response bad formatted",
  cors: "Not allowed by CORS",
});
