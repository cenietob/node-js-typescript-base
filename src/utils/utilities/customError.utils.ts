export class ErrorResponse extends Error {
  /**
   * Create a Error object.
   * @param {Object} httpStatus Http status of the error.
   * @param {String} description Description of the error.
   * @param {Boolean} isOperational If the error thrown is operational or not
   * @param {String} additionalMessage Additional message that wants to be concatenated to the
   * description.
   */

  statusCode: number;

  constructor(
    private httpStatus: { name: string; code: number },
    private developerDescription: string,
    private userDescription: string | undefined = "",
    private isOperational: boolean = false,
  ) {
    super(userDescription);
    this.name = httpStatus.name;
    this.statusCode = httpStatus.code;
  }
}
