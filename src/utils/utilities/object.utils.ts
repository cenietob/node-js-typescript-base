/**
 * Get the value of a field in an object.
 * @param {T} o Object that will be consulted.
 * @param {K} propertyName Field that will be consulted.
 * @returns {any} The value of the field in the object.
 */
export function getProperty<T, K extends keyof T>(o: T, propertyName: K): T[K] {
  return o[propertyName];
}

/**
 * Update a object modifying one property.
 * @param {T} o Object that will be updated.
 * @param {K} propertyName Field that will be updated.
 * @param {any} value Value that will be set in the object.
 */
export function setProperty<T, K extends keyof T>(
  o: T,
  propertyName: K,
  value: any,
) {
  o[propertyName] = value;
}
