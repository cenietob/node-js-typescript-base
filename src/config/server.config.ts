import * as dotenv from "dotenv";
dotenv.config();

export const config = Object.freeze({
  whitelist: (process.env.WHITELIST || "*").split(",").filter((host) => !!host),
  nodeEnv: process.env.NODE_ENV,
  port: process.env.PORT,

  api: {
    prefix: "/api",
  },

  logs: {
    level: process.env.LOG_LEVEL || "silly",
    dateFormat: process.env.LOG_DATE_FORMAT || "YYYY-MM-DD HH:mm:ss",
  },
});
