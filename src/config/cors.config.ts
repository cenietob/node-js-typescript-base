import * as cors from "cors";

import { config } from "@config/server.config";
import { ErrorResponse } from "@utils/utilities";
import { commonErrors, serverErrors } from "@utils/constants";

export const corsOptions: cors.CorsOptions = {
  // allowedHeaders: [
  //   "Origin",
  //   "X-Requested-With",
  //   "Content-Type",
  //   "Accept",
  //   "X-Access-Token",
  // ],
  // credentials: true,
  // methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
  origin: function (origin: any, callback: Function) {
    const all = config.whitelist.indexOf("*") !== -1;
    if (all || config.whitelist.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(
        new ErrorResponse(
          serverErrors.unauthorized,
          commonErrors.cors,
          undefined,
          true,
        ),
        false,
      );
    }
  },
  // preflightContinue: false,
};
