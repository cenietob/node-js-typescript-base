#!/usr/bin/env node
import http from "http";
import "colors";

import appLoader from "@loaders";
import { config } from "@config/index";

import express from "express";

const debug = require("debug")("quote-portal:server");
const port = normalizePort(config.port || "8081");

/**
 * Start the node server.
 */
async function startServer() {
  const app = express();

  appLoader({ expressApp: app });

  const port = normalizePort(config.port || "8081");
  app.set("port", port);

  const server = http.createServer(app);

  server.listen(port, () =>
    console.log(
      `Server running in ${config.nodeEnv} mode on port ${port}`.yellow.bold,
    ),
  );
  server.on("error", onError);
  server.on("listening", () => onListening(server));
}

startServer();

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort(val: string) {
  const port = parseInt(val, 10);
  if (isNaN(port)) return val;
  if (port >= 0) return port;
  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error: { syscall: string; code: any }) {
  if (error.syscall !== "listen") {
    throw error;
  }

  const bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening(server: http.Server) {
  const addr = server.address();
  const bind = typeof addr === "string" ? "pipe " + addr : "port " + port;
  debug("Listening on " + bind);
}
