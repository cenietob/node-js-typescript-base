export interface ServerError {
  name: string;
  code: number;
}
