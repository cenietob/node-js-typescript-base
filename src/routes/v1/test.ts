import { Mutable } from "@interfaces/index";
import { Request, Response, NextFunction, Router } from "express";

const router = Router();

router.get("/", function (req, res) {
  res.json({ APIRunning: true });
});

router.get(
  "/test",
  (req: Request, res: Response, next: NextFunction) => {
    try {
      console.log("here2");
      res.locals = { response: {} };
      res.locals = {
        response: {
          statusCode: 200,
          body: {
            success: true,
            msg: "pass",
            data: {},
          },
        },
      };
      next();
    } catch (error) {
      next(error);
    }
  },
);

export default router;
