import express from "express";

import v1Router from "@routes/v1";
import healthCheckRouter from "@routes/healthCheck.routes";

const router = express.Router();

router.use(healthCheckRouter);
router.use("/v1", v1Router);

export default router;
