import express from "express";

const router = express.Router();

router.get("/status", (req, res) => {
  res.status(200).send({ data: "hi" });
});
router.head("/status", (req, res) => {
  res.status(200).end();
});

export default router;
